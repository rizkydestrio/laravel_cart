<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Cart;
use App\Product;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Cart::content());
        return view('shop.cart');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // Cek color
        if($request->color!=Null){
           $color = $request->color;
        }else{
          $color = Null;
        }
        // Cek size
        if($request->prdsize!=Null){
          $size = $request->prdsize;
        }else{
          $size = Null;
        }
        // Cek QTY
        if($request->quantity!=Null){
          $quantity = $request->quantity;
        }else{
          $quantity = Null;
        }
        Cart::add($request->id,$request->name,$quantity,$request->price,
                 ['prdthumb'=>$request->prdthumb,'color'=>$color,'size'=>$size ])->associate('App\Product');  // add itu function dari gloudemans cart,  associate Memanggil model product //
        return redirect('cart')->with('sukses','Produk Berhasil Ditambahkan Ke Keranjang Belanja');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
       $validasi = Validator::make($request->all(),[
              'quantity' => 'required|numeric|between:1,5'
       ]);
       if($validasi->fails()){
         return redirect('cart')->with('gagal','Minimal quantity 1 dan Maksimal quantity 5');
       }
        Cart::update($id, $request->quantity);
        return redirect('cart')->with('update','Quantity berhasil di update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Cart::remove($id);
        return redirect('cart')->with('gagal','Produk berhasil di hapus dari keranjang belanja');
    }
}
