<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Order_detail;
use Cart;
use Auth;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
      $this->middleware('auth');
    }
    public function index()
    {
        //
        return view('shop.checkout');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd(Cart::content());
        $orderid = Order::create([
           'ordersidencytpe' => substr(bcrypt(1), 13, 17),
           'usersid'=> Auth::user()->id,
           'ordfullname' => $request->ordfullname,
           'ordmail' => $request->ordmail,
           'ordnohp' => $request->ordnohp,
           'ordalamat' => $request->ordalamat,
           'ordkota' => $request->ordkota,
           'ordprovinsi' => $request->ordprovinsi,
           'ordbyr' => Cart::subtotal(),
           'ordstatus' => "N",
           'ordmethod' => $request->ordmethod,
        ]);

        foreach( Cart::content() as $item )
        {
          if($item->options->color!=Null){
            $color = $item->options->color;
          }else{
            $color = "Null";
          }
          if($item->options->size!=Null){
            $size = $item->options->size;
          }else{
            $size = "Null";
          }
          Order_detail::create([
             'ordersid' => $orderid->id,
             'prdid' => $item->id,
             'qty' => $item->qty,
             'warna' => $color,
             'size' => $size,
          ]);
        }

        Cart::destroy();

        return redirect('thanks')->with('sukses','Terimakasih Sudah Belanja di toko Kami!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
