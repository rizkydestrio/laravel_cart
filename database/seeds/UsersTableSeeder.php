<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Rizky Destrio',
            'email' => 'rizkydestrio@gmail.com',
            'password' => bcrypt('rizkydestrio'),
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
