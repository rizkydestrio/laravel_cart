<?php

// Toko Controller
Route::get('/','TokoController@index');
Route::get('beli/{id}','TokoController@show');
Route::get('toko','TokoController@product');
Route::get('thanks','TokoController@thanks');
Route::resource('cart','CartController');
Route::resource('checkout','CheckoutController');
Route::resource('myaccount','AccountController');

Route::auth();
