<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable=['prdname','prdcatid','prdslug','prdsize','prdprice','prddesc','prdstock','prdthumb'];
}
